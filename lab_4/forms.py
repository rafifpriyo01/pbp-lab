from django import forms

import sys
# from ..lab_2 import models\
sys.path.insert(0, '/pbp-lab/lab_2')
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ('to', 'from_var', 'title', 'message',)