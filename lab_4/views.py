from django.shortcuts import render, HttpResponseRedirect
from .forms import NoteForm

import sys
# from ..lab_2 import models\
sys.path.insert(0, '/pbp-lab/lab_2')
from lab_2.models import Note

# Create your views here.

def index(request):
    lst_of_notes = Note.objects.all().values()
    response = {'lst_of_notes' : lst_of_notes,}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    response = {}

    form = NoteForm(request.POST)

    if form.is_valid():
        form.save()

        return HttpResponseRedirect('.')

    response['form'] = form
    return render(request, "lab4_form.html", response)

def note_list(request):
    lst_of_notes = Note.objects.all().values()
    response = {'lst_of_notes' : lst_of_notes,}
    return render(request, 'lab4_note_list.html', response)