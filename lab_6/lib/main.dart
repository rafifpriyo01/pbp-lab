import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Laman APD',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      // home: const MyHomePage(title: 'PBP D-05'),
      home: MyStatelessWidget(judul: 'PBP D-05'),
    );
  }
}

/// This is the stateless widget that the main application instantiates.
class MyStatelessWidget extends StatelessWidget {
  MyStatelessWidget({Key? key, required this.judul}) : super(key: key);

  final String judul;

  Widget textSectionJudul = Container(
    padding: EdgeInsets.all(32),
    child: Text(
      'Laman APD',
      softWrap: true,
      style: TextStyle(fontSize: 24),
      textAlign: TextAlign.left,
    ),
  );

  Widget textSection1 = Container(
    padding: EdgeInsets.only(left: 32, right: 32),
    child: RichText(
      text: TextSpan(
        children: <TextSpan>[
          TextSpan(
            text: 'Alat Pelindung Diri (APD) adalah kelengkapan yang wajib digunakan '
            'Alat Pelindung Diri (APD) adalah kelengkapan yang wajib digunakan '
            'itu sendiri dan orang di sekelilingnya. (',
          ),
          TextSpan(
            text: 'id.wikipedia.org',
          ),
          TextSpan(text: ')')
        ]
      ),
    ),
  );

  Widget cardSection = Container(
    padding: EdgeInsets.all(32),
    color: Colors.blue,
    child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              flex: 3,
              child: Container(
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all()
                ),
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text("Spoundbound", style: TextStyle(fontSize: 18), )
                    ),
                    SizedBox(height: 8,),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Apd berjenis Spoundbound "
                        "dengan harga Rp.108000",
                        style: TextStyle(fontSize: 12),
                      )
                    ),
                    SizedBox(height: 8,),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "dapat di pesan di:",
                        style: TextStyle(fontSize: 12),
                      )
                    ),
                    SizedBox(height: 4,),
                    Align(
                      alignment: Alignment.topLeft,
                      child: ElevatedButton(onPressed: () {}, child: Text("DISINI"))
                    )
                  ],
                ),
              ),
            ),
            SizedBox(width: 16,),
            Expanded(
              flex: 3,
              child: Container(
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all()
                ),
                child: Column(
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text("Parasut", style: TextStyle(fontSize: 18)),
                    ),
                    SizedBox(height: 8,),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Apd berjenis Parasut "
                        "dengan harga Rp.120000",
                        style: TextStyle(fontSize: 12),
                      )
                    ),
                    SizedBox(height: 8,),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "dapat di pesan di:",
                        style: TextStyle(fontSize: 12),
                      )
                    ),
                    SizedBox(height: 4,),
                    Align(
                      alignment: Alignment.topLeft,
                      child: ElevatedButton(onPressed: () {}, child: Text("DISINI"))
                    )
                  ]
                )
              )
            ),
            SizedBox(width: 16,),
            Expanded(
            flex: 3,
            child: Container(
              padding: EdgeInsets.all(16),
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all()
              ),
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text("Taslan", style: TextStyle(fontSize: 18))
                  ),
                  SizedBox(height: 8,),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                    "Apd berjenis Taslan "
                    "dengan harga Rp.144000",
                    style: TextStyle(fontSize: 12),
                  )
                  ),
                  SizedBox(height: 8,),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                    "dapat di pesan di:",
                    style: TextStyle(fontSize: 12),
                  ),
                  ),
                  SizedBox(height: 4,),
                  Align(
                    alignment: Alignment.topLeft,
                    child: ElevatedButton(onPressed: () {}, child: Text("DISINI"))
                  )
                ],
              ),
            )
            ),
          ],
        ),
        SizedBox(height: 16,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
              flex: 3,
            child: Container(
              padding: EdgeInsets.all(16),
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all()
              ),
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text("Protective", style: TextStyle(fontSize: 18)),
                  ),
                  SizedBox(height: 8,),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "Apd berjenis Protective "
                      "dengan harga Rp.499900",
                      style: TextStyle(fontSize: 12),
                    )
                  ),
                  SizedBox(height: 8,),
                  Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      "dapat di pesan di:",
                      style: TextStyle(fontSize: 12),
                    )
                  ),
                  SizedBox(height: 4,),
                  Align(
                    alignment: Alignment.topLeft,
                    child: ElevatedButton(onPressed: () {}, child: Text("DISINI"))
                  )
                ],
              ),
            )
            ),
            SizedBox(width: 16,),
            Expanded(
              flex: 3,
            child: Container(
              padding: EdgeInsets.all(16),
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all()
              ),
              child: Align(
                alignment: Alignment.topLeft,
              child: Column(
              ),
            )
            )
            ),
            SizedBox(width: 16,),
            Expanded(
              flex: 3,
            child: Container(
              padding: EdgeInsets.all(16),
              decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all()
              ),
              child: Align(
                alignment: Alignment.topLeft,
              child: Column(
              ),
            )
            )
            )
          ],
        )
      ],
    )
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // title: const Text('AppBar Demo'),
        title: Text(judul),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.home),
            tooltip: 'Beranda',
            onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('This is Beranda')));
            },
          ),
          IconButton(
            icon: const Icon(Icons.apps),
            tooltip: 'Fitur',
            onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('This is Fitur')));
            },
          ),
          IconButton(
            icon: const Icon(Icons.speaker_notes),
            tooltip: 'Forum',
            onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('This is Forum')));
            },
          ),
          IconButton(
            icon: const Icon(Icons.help),
            tooltip: 'FAQ',
            onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('This is FAQ')));
            },
          ),
        ],
      ),
      // body: const MyTable(),
      body: ListView(
        children: [
          textSectionJudul,
          textSection1,
          Container(
            padding: EdgeInsets.all(32),
            alignment: Alignment.centerRight,
            child: ElevatedButton(
              onPressed: () {},
              child: const Text('Tambah APD', textAlign: TextAlign.center,),
            ),
          ),
          cardSection,
        ],
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
