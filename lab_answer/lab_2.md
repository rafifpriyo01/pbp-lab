1. Apakah perbedaan antara JSON dan XML?

    Perbedaan keduanya tidak jauh berbeda sepertinya, mereka sama-sama data transfer?
    Jika JSON, data yang ditampilkan apa adanya, yaitu dalam bentuk key: value
        Makanya, disarankan ketika membuat name var gunakan nama yang mudah dimengerti
    Berbeda dengan XML, XML menampilkan datanya dalam bentuk yang lebih kompleks, tetapi lebih terstruktur, sepertinya...
        Seperti ada didalam tag-tag <></>, setiap elemen objek berada di dalam tag <object></object> nya
    Itulah perbedaan yang paling mencolok dalam lab-2 ini.


2. Apakah perbedaan antara HTML dan XML?

    Yaa jelas berbeda, yang satu (HTML) untuk menampilkan data dengan cara yang enak dilihat, ada desain-desainnya
    sedangkan yang satunya lagi (XML) bertugas untuk menyampaikan atau menyimpan data, tetapi dalam tempat yang terstruktur.

    Keduanya dapat terlihat di dalam pengerjaan lab-2 ini.