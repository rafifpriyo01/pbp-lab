from django.shortcuts import render, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from datetime import datetime, date
from .forms import FriendForm
from .models import Friend

# Create your views here.

@login_required(login_url = '/admin/login/') #Jika ini ditambahkan, maka form akan terbuka jika sudah login
def index(request):
    lst_of_friends = Friend.objects.all().values()  # TODO Implement this
    response = {#'name' : friends.name,
                #'npm' : friends.npm,
                #'birth_date' : friends.birth_date,}
                'lst_of_friends' : lst_of_friends,}
    return render(request, 'lab3_index.html', response)

@login_required(login_url = '/admin/login/')
def add_friend(request):
    response = {}

    form = FriendForm(request.POST)

    if form.is_valid():
        form.save()

        return HttpResponseRedirect('.')

    response['form'] = form
    return render(request, "lab3_form.html", response)