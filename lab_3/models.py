from django.db import models
from datetime import datetime, date

# Create your models here.

class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.IntegerField('npm is number')
    birth_date = models.DateField('date of birth')
    # TODO Implement missing attributes in Friend model

    def __str__(self):
        return self.name
