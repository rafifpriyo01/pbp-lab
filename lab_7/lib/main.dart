import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const FirstRoute(title: 'Flutter Demo Home Page'),
    );
  }
}

class FirstRoute extends StatelessWidget {
  const FirstRoute({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // title: const Text('AppBar Demo'),
        title: Text(title),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.home),
            tooltip: 'Beranda',
            onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('This is Beranda')));
            },
          ),
          IconButton(
            icon: const Icon(Icons.apps),
            tooltip: 'Fitur',
            onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('This is Fitur')));
            },
          ),
          IconButton(
            icon: const Icon(Icons.speaker_notes),
            tooltip: 'Forum',
            onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('This is Forum')));
            },
          ),
          IconButton(
            icon: const Icon(Icons.help),
            tooltip: 'FAQ',
            onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('This is FAQ')));
            },
          ),
        ],
      ),
      body: Center(
        child: ElevatedButton(
          child: Text('Open route'),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => SecondRoute(title: title)),
              // MaterialPageRoute(builder: (context) => SecondRouteInner()),
            );
          }
        ),
      ),
    );
  }
}

class SecondRoute extends StatelessWidget {
  SecondRoute({Key? key, required this.title}) : super(key: key);

  final String title;
  final _formKey = GlobalKey<FormState>();

  Widget textSectionJudul = Container(
    padding: EdgeInsets.all(32),
    child: Text(
      'Form Menambahkan Penyedia APD',
      softWrap: true,
      style: TextStyle(fontSize: 24),
      textAlign: TextAlign.left,
    ),
  );

  Widget textSection1 = Container(
    padding: EdgeInsets.only(left: 32, right: 32, bottom: 16),
    child: Text(
      'Jika Anda mengetahui situs penyedia APD, ataupun '
      'ingin berbagi link, silahkan isi form ini',
      softWrap: true,
      style: TextStyle(fontSize: 18),
      textAlign: TextAlign.left,
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // title: const Text('AppBar Demo'),
        title: Text(title),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.home),
            tooltip: 'Beranda',
            onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('This is Beranda')));
            },
          ),
          IconButton(
            icon: const Icon(Icons.apps),
            tooltip: 'Fitur',
            onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('This is Fitur')));
            },
          ),
          IconButton(
            icon: const Icon(Icons.speaker_notes),
            tooltip: 'Forum',
            onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('This is Forum')));
            },
          ),
          IconButton(
            icon: const Icon(Icons.help),
            tooltip: 'FAQ',
            onPressed: () {
              ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(content: Text('This is FAQ')));
            },
          ),
        ],
      ),
      // body: SingleChildScrollView( child: Column(
      //   children: [
      //     Align(
      //       alignment: Alignment.topLeft,
      //       child: textSectionJudul,
      //     ),
      //     Align(
      //       alignment: Alignment.topLeft,
      //       child: textSection1,
      //     ),
      //     Expanded(
      //       child: SecondRouteInner(),
      //     ),
        // ],
    //   ))
    // );
      body: SecondRouteInner(),
    );
  }
}

class SecondRouteInner extends StatefulWidget {
  const SecondRouteInner({ Key? key }) : super(key: key);

  @override
  State<SecondRouteInner> createState() => _SecondRouteInnerState();
}

class _SecondRouteInnerState extends State<SecondRouteInner> {
  final _formKey = GlobalKey<FormState>();

  Widget textSectionJudul = Container(
    padding: EdgeInsets.all(32),
    child: Text(
      'Form Menambahkan Penyedia APD',
      softWrap: true,
      style: TextStyle(fontSize: 24),
      textAlign: TextAlign.left,
    ),
  );

  Widget textSection1 = Container(
    padding: EdgeInsets.only(left: 32, right: 32, bottom: 16),
    child: Text(
      'Jika Anda mengetahui situs penyedia APD, ataupun '
      'ingin berbagi link, silahkan isi form ini',
      softWrap: true,
      style: TextStyle(fontSize: 18),
      textAlign: TextAlign.left,
    ),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(child: Form(
        key: _formKey,
        child: Column(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: textSectionJudul,
            ),
            Align(
              alignment: Alignment.topLeft,
              child: textSection1,
            ),
            Container(
              color: Colors.blue,
              padding: const EdgeInsets.only(left: 32, right: 32, top: 16, bottom: 16),
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      decoration: new InputDecoration(
                        fillColor: Colors.white,
                        filled: true,
                        hintText: "Format: Jenis_APD Penyedia_APD",
                        labelText: "Jenis APD",
                        icon: Icon(Icons.people),
                        border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Jenis APD tidak boleh kosong';
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      decoration: new InputDecoration(
                        fillColor: Colors.white,
                        filled: true,
                        hintText: "URL",
                        labelText: "URL lokasi penyedia APD",
                        icon: Icon(Icons.room),
                        border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'URL tidak boleh kosong';
                        }
                        return null;
                    } ,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      decoration: new InputDecoration(
                        fillColor: Colors.white,
                        filled: true,
                        hintText: "Format: Angka saja tanpa titik",
                        labelText: "Harga APD",
                        icon: Icon(Icons.paid),
                        border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Harga APD tidak boleh kosong';
                        }
                        return null;
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      decoration: new InputDecoration(
                        fillColor: Colors.white,
                        filled: true,
                        hintText: "Format: Isi # Jika kosong",
                        labelText: "Gambar APD dalam bentuk URL",
                        icon: Icon(Icons.info),
                        border: OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(5.0)),
                      ),
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return null;
                        }
                        return null;
                      },
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Padding( 
                padding: const EdgeInsets.all(32),
                child: ElevatedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      Navigator.pop(context);
                    }
                  },
                ),
              )
            )
          ]
        )
      )),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
